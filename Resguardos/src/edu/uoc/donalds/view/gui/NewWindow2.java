package edu.uoc.donalds.view.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Clase Ventana
 * Muestra la estructuta que deberia tener una Ventana en Java con la libreria
 * Swing, contiene una etiqueta, un caja de texto y un boton, que tiene la
 * accion de mostrar el texto en la caja por una ventana de mensaje.
 * @author Daniel Alvarez (a3dany)
 */
public class NewWindow2 extends JFrame implements ActionListener {
	

    private JLabel texto;           // etiqueta o texto no editable
    private JTextField caja;        // caja de texto, para insertar datos
    private JButton button;          // boton con una determinada accion

    public NewWindow2() {
        super();                    // usamos el contructor de la clase padre JFrame
        configurarVentana();        // configuramos la ventana
        inicializarComponentes();   // inicializamos los atributos o componentes
    }

    private void configurarVentana() {
        this.setTitle("Item name");                   // colocamos titulo a la ventana
        this.setSize(300, 210);                                 // colocamos tamanio a la ventana (ancho, alto)
        this.setLocationRelativeTo(null);                       // centramos la ventana en la pantalla
        this.setLayout(null);                                   // no usamos ningun layout, solo asi podremos dar posiciones a los componentes
        this.setResizable(false);                               // hacemos que la ventana no sea redimiensionable
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // hacemos que cuando se cierre la ventana termina todo proceso
    }

    private void inicializarComponentes() {
        // creamos los componentes
    	
        texto = new JLabel();
        //caja = new JTextField();
        button = new JButton();
        // configuramos los componentes
        //texto.setText("Niidea");    // colocamos un texto a la etiqueta
        texto.setText("<html>First ingredient<p>Second ingredient<br>"
        		+ "Vegan or not<p> Other information about</html>");    // Esto es mio
        texto.setBounds(50, 60, 500, 25);   // colocamos posicion y tamanio al texto (x, y, ancho, alto)
        //caja.setBounds(150, 50, 100, 25);   // colocamos posicion y tamanio a la caja (x, y, ancho, alto)
        button.setText("Close");   // colocamos un texto al boton
        button.setBounds(120, 110, 80, 30);  // colocamos posicion y tamanio al boton (x, y, ancho, alto)
     
        button.addActionListener(this);      // hacemos que el boton tenga una accion y esa accion estara en esta clase
        // adicionamos los componentes a la ventana
        
        this.add(texto);
        //this.add(caja);
        this.add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       	this.dispose();
    }

    public static void main(String[] args) {
    //public void openWindow(){
    	NewWindow2 V = new NewWindow2(); // creamos una ventana
        V.setVisible(true);             // hacemos visible la ventana creada
    }
}