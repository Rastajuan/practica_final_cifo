package edu.uoc.donalds.model;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * This class represents an UocDonald's order.
 * 
 * @author David Garc�a Sol�rzano
 * @version 1.0
 * 
 */

public class Order {
	
	private static int id = 0;
	private DiningLocation diningLocation;
	private List<Item> items;
	private boolean confirmOrder;
	
	
	/**
	  * Default constructor which sets id++, diningLocation equals to EATIN and set items with 15 positions.
	 *	   
	 */
	public Order() {
		//increase1Id();
		diningLocation = DiningLocation.EATIN;
		items = new LinkedList<Item>();
		setConfirmOrder(false); //When creating the order it is not confirmed, so we initialize it to false
	}

	/**
	 * Returns if the order is confirmed
	 * 
	 * @return true if Order is confirmed, false if not
	 */
	public boolean isConfirmOrder() {
		return confirmOrder;
	}


	/**
	 * Set a new value for the private attribute confirmOrder
	 * 
	 * @param confirmOrder New value for the private field "confirmOrder"
	 */
	public void setConfirmOrder(boolean confirmOrder) {
		this.confirmOrder = confirmOrder;
	}
	
	/**
	 * Returns the order's id.
	 * 
	 * @return Order's number/id.
	 */
	public int getId() {
		return id;
	}


	/**
	 * Sets a new value for the Order's id.
	 * 
	 * @param id New value for the private field "id".
	 */
	public void setId(int id) {
		Order.id = id;
	}


	/**
	 * Returns where the customer will eat her order.
	 * 
	 * @return Dining location's value.
	 */
	public DiningLocation getDiningLocation() {
		return diningLocation;
	}


	/**
	 * Sets where the customer will eat her order.
	 * 
	 * @param diningLocation Location/Place where the customer will eat her order.
	 */
	public void setDiningLocation(DiningLocation diningLocation) {
		this.diningLocation = diningLocation;
	}

	
	/**
	 * Increases 1 unit the value of the private field "id".
	 */
	private void increase1Id(){
		setId(getId()+1); //tambien valdria "id++", pero usando el getter y setter se mejora el mantenimiento ante posibles cambios.
	}
	
	/**
	 * Returns the whole list of items.
	 * 
	 * @return The private field "items".
	 */
	public List<Item> getItems() {
		return items;
	}


	/**
	 * Replaces the whole list of items.
	 *	 
	 * @param items New list of items.
	 */
	public void setItems(List<Item> items) {
		this.items = items;
	}
	

	/**
	 * Adds a new item to the end of the list "items".
	 * 
	 * @param item New item to add.
	 * 
	 * @throws OrderException if the order has already been committed or you try to add an item exhausted or that will be exhausted if the order is confirmed.
	 * @throws ItemException if there is some problems with decreaseStock() or increase1ExpectedPurchase().
	 */
	public void addItem(Item item) throws OrderException, ItemException{
		if(isConfirmOrder()) {
			throw new OrderException("The order has already been committed!!! You cannot add a new item!!");
			
		}else if(item.isSoldOut() || (item.getStock() == 0 && !isConfirmOrder())) {
			 	throw new OrderException("The item is sold out!!! You cannot add it to your order!!!");

			}else {
				items.add(item);
				item.decrease1Stock();
				item.increase1ExpectedPurchase();
			}
				
	}
	
	/**
	 * Removes the item in the position "index" of the list "items".
	 * 
	 * @param index Position where the item we want to remove is.
	 * 
	 * @throws OrderException if the order has already been committed
	 * 
	 * @throws ItemException if there is a problem with setStock ()
	 */
	public void removeItem(int index) throws OrderException, ItemException{	
		if(isConfirmOrder()) {
			throw new OrderException("The order has already been committed!!! You cannot remove an item!!");
		} else {
			items.get(index).decrease1ExpectedPurchase();
			items.get(index).setStock(items.get(index).getStock() + 1 );
			items.remove(index);
		}
	}

	
	/**
	  * This method adds the gross price of all the items of the order together.
	 * 
	 * @return The sum in euros of the gross price of the order's items.  
	 */
	public double getTotalGrossCost(){
		double total = 0;
		
		for(Item item : items){
			//total += (item!=null)?item.getGrossPrice():0;
			total += item.getGrossPrice(); //quitamos la comprobacion de si es null, porque ahora no tiene sentido.
		}
		
		return total;
		
	}
	
	/**
	  * This method adds the net price of all the items of the order together.
	 * 
	 * @return The sum in euros of the net price of the order's items.  
	 */
	public double getTotalNetCost(){
		double total = 0;
		
		for(Item item : items){
			//total += (item!=null)?item.getNetPrice():0;
			total += item.getNetPrice(); //quitamos la comprobacion de si es null, porque ahora no tiene sentido.
		}
		
		return total;
		
	}

	/**
	 * This method adds the difference between the gross price and the net price of all the items of the order together.
	 * 
	 * @return The sum in euros of the taxes of the order's items.  
	 */
	public double getTotalTaxesCost(){
		double total = 0;
		
		for(Item item : items){
			//total += (item!=null)?(item.getGrossPrice()-item.getNetPrice()):0;
			total += item.getGrossPrice()-item.getNetPrice(); //quitamos la comprobacion de si es null, porque ahora no tiene sentido.
		}
		
		return total;
	}

	
	/**
	 * This method confirms the order.
	 * 
	 * @throws OrderException if the order has already been confirmed or if it is empty.
	 * 
	 * @throws ItemException by the Item's method decrease1Stock.
	 * 
	 * @throws IOException if there is an error in the printReceipt () method
	 */
	public void commit() throws OrderException, ItemException, IOException{
		
		//If the order has already been confirmed or the order is empty
		if(confirmOrder && items.size() > 0) {
			throw new OrderException("The order has already been commited!!!");
		}
		if(items.size() == 0) {
			throw new OrderException("Your order is empty, then you cannot commit it!!!");
		}else
		
		// If the order has not been confirmed yet, we confirm it and update the parameters "decreaseStock" and "increaseId"
		if(!confirmOrder) {
			for(Item items : items) {
				if(items.getStock() > 0) {
					items.decrease1Stock();
				}
				items.decrease1ExpectedPurchase();
				}
			this.setConfirmOrder(true);
			this.increase1Id();
			printReceipt();
			
		}
		
	}

	
	
	/**
	 * This method prints the order invoice
	 * 
	 * @throws IOException or OrdeException if there is any problem in the treatment of the file
	 */
	private void printReceipt() throws IOException {
		
		//Creation and writing of the file
		try
		{
			//Create a File object is responsible for creating or opening access to a file that is specified in its constructor
			File file = new File("output\\receipt.txt");

			//Create a FileWriter object that will help us write about file
			FileWriter writer=new FileWriter(file,true);

			//We write in the file 			
			writer.write(this.toString());;

			//We close the connection
			writer.close();
		} 
		catch(IOException e) //If there is a problem writing the file
		{
			throw new IOException("Error writing the file");
			
		}
		
		//File printing
		//We create a String that will contain all the text of the file
		String container ="";

		try
		{
			//We create a FileReader file that gets what the file has
			FileReader fReader = new FileReader("output\\receipt.txt");

			//The reader content is saved in a BufferedReader
			@SuppressWarnings("resource")
			BufferedReader bfReader = new BufferedReader(fReader);

			//With the following cycle we extract all the content of the object "receipt" and show it
			while((container = bfReader.readLine())!=null){
				System.out.println(container);
			}
		}catch(IOException e){
			throw new IOException("Error reading the file");
		}
		

	}
	
	/**
	 * This method overrides Object's toString.
	 * 
	 * @return String with the Order's id, the list of the items with the format "name......grosPrice �", and the total gross cost and total taxes cost.
	 */
	@Override
	public String toString(){
		DecimalFormat df = new DecimalFormat("0.00");
		StringBuilder text = new StringBuilder();
			
		text.append("Dining location: "+getDiningLocation()+"");
		text.append("\n__________________________\n");
		
		for(Item item : items){
			text.append("\n"+item); //aprovechamos el m�todo toString de Item que ya devuelve el String en el formato que necesitamos.
			//Adem�s, gracias al uso de una List, ya no hay que comprobar si es null o no
		}
		
		text.append("\n__________________________\n");
			
		text.append("\nTOTAL: "+df.format(getTotalGrossCost())+" �");
		text.append("\nTaxes: "+df.format(getTotalTaxesCost())+" �");
		text.append("\n__________________________\n");
		
		return text.toString();	
	}
}
