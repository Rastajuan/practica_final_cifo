package edu.uoc.donalds.model;

/**
 * This class represents a salad of the main course of the UocDonald's menu
 * 
 * @author Juan Bello
 * 
 *@version 1.0
 */
public class Salad extends MainCourse{
	
	private boolean isVegan;
	
	//Constructors
	/**
	 * Default Constructor
	 * 
	 * @throws ItemException if there is some problem with the Item constructor
	 */
	public Salad() throws ItemException {
		super();
		setVegan(false);
	}
	
	/**Constructor with parameters
	 * 
	 * @param name Salad's name.
	 * @param isHot It will always be false.	
	 * @param imageSrc The source of the salad's image.
	 * @param grossPrice Salad's gross price.
	 * @param tax Salad's tax.
	 * @param kcal Salad's kcal.
	 * @param stock Number of salads of this type that the restaurant has.
	 * @param isVegan It is true if the salad is suitable for vegans, otherwise (i.e. cold) it is false.
	 * @throws ItemException if there is some problem with the Item constructor
	 */
	public Salad(String name, boolean isHot, String imageSrc, double grossPrice, double tax, double kcal, int stock) throws ItemException{
		super(name, isHot, imageSrc, grossPrice, tax, kcal, stock);
		setHot(false); //We change the default value of isHot to false
		setVegan(false);
	}
	
	//Setter & Getter isVegan
	/**
	 * Returns the current value of the private field "isVegan".
	 * 
	 * @return true if it is suitable for vegans, false if not.
	 */
	public boolean isVegan() {
		return isVegan;
	}

	/**
	 * Replaces the current value of the private field "isVegan".
	 * 
	 * @param isVegan New value for the private field "isVegan".
	 */
	private void setVegan(boolean isVegan) { //We declare it 'private' because it can not be modified outside of class
		this.isVegan = isVegan;
	}

}

 