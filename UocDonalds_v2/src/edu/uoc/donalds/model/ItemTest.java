package edu.uoc.donalds.model;

import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ItemTest {
	
	@Rule  
	public ExpectedException thrown = ExpectedException.none();
	
	/**
	 * This method performs a unit test of the setStock () method of the Item class
	 * 
	 * @throws ItemException if the value of setStock() is negative.
	 */
	@Test
	
	public void testSetStock() throws ItemException {
			
		//Instance an Item object, in this case an item "Burger"
		Item instanceItem = new Burger();
		
		//Assign a positive value with the method to be tested
		instanceItem.setStock(3);
		
		/*We use the assertEquals () method for checking: the first parameter is the value to be compared with the assignment we made with the setStock() method, 
		 * the second parameter returns the value of the stock with getStock() and compares it with the first*/
		assertEquals(3,instanceItem.getStock(), 0.0);

	    //We add the expected exception and assign the negative value that the release	
		 thrown.expect(ItemException.class);         
		 thrown.expectMessage("Stock cannot be a negative value!!");
		 instanceItem.setStock(-1); 

	}
}