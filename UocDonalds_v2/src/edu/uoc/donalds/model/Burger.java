package edu.uoc.donalds.model;

/**This class represents a burger of the main course of the UocDonald's menu.
 * 
 * @author Juan Bello
 * @version 1.0
 */
public class Burger extends MainCourse {
	
	private boolean isGlutenFree;

	/**Default Constructor
	 * @throws ItemException is thrown if there is any problem with the super constructor 
	 */
	//Constructors
	public Burger() throws ItemException {
		super();
		setGlutenFree(false);
	}
	
	/**Constructor with parameters
	 * 
	 * @param name Burger's name.
	 * @param isHot It will always be true.	
	 * @param imageSrc The source of the burger's image.
	 * @param grossPrice Burger's gross price.
	 * @param tax Burger's tax.
	 * @param kcal Burger's kcal.
	 * @param stock Number of burger of this type that the restaurant has.
	 * @throws ItemException is thrown if there is any problem with the super constructor 
	 */
	public Burger(String name, boolean isHot, String imageSrc, double grossPrice, double tax, double kcal, int stock) throws ItemException{
		super(name, isHot, imageSrc, grossPrice, tax, kcal, stock);
		setGlutenFree(false);
	}

	//Setter & Getter isGlutenFree
	/**
	 * Returns the current value of the private field "isGlutenFee".
	 *
	 * @return The current value of the private field "isGlutenFee".
	 */
	public boolean isGlutenFree() {
		return isGlutenFree;
	}

	/**
	 * Replaces the current value of the private field "isGlutenFree".
	 * 
	 * @param isGlutenFree set the new value for the private field "isGlutenFee"
	 */
	private void setGlutenFree(boolean isGlutenFree) { 	//We declare it 'private' because it can not be modified outside of class
		this.isGlutenFree = isGlutenFree;
	}
	

	
}

