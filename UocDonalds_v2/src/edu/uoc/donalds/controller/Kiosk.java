package edu.uoc.donalds.controller;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import edu.uoc.donalds.model.*;
import edu.uoc.donalds.utils.Inventory;

/**
 * This class represents a kiosk (entry point class).
 * 
 * @author David Garc�a Sol�rzano
 * @version 1.0
 *  
 */
public class Kiosk {
	
	private Order order;
	private Inventory inventory;
	
	/**
	 * Constructor.
	 */
	public Kiosk() {
		this("");
	}
	
	
	/**
	 * Constructor with one argument.
	 * @param fileNameInventory Path to the file that contains inventory's data.
	 */
	public Kiosk(String fileNameInventory){
		if(fileNameInventory.isEmpty()){
			inventory = new Inventory(); //We cannot use "this();", because a constructor call must be the first statement in a constructor
		}else{
			inventory = new Inventory(fileNameInventory);
		}
	}
	
	
	/**
	 * Creates a new order object.
	 */
	public void createOrder(){
		order = new Order();
	}
	
	/**
	 * Sets the dining location for the current order.
	 * @param diningLocation an enum with the value "EATIN" or "TAKEAWAY"
	 */
	public void setDiningLocation(DiningLocation diningLocation){
		order.setDiningLocation(diningLocation);
	}

	/**
	 * Sets a new item
	 * @param item New item to add to the order.
	 * @throws OrderException is thrown when the item to add is sold out or the order is committed.
	 * @throws ItemException is trhown by addItem() method of Item class
	 */
	public void addItem2Order(Item item) throws OrderException, ItemException{
		order.addItem(item);
	}
	
	/**
	 * Removes the item in the position "index" from the order.
	 * @param index Position of the item to remove.
	 * @throws OrderException When the index is out of the bounds of the list or the order is committed.
	 * @throws ItemException is trhown by removeItem() method of Item class
	 */
	public void removeItemFromOrder(int index) throws OrderException, ItemException{
		order.removeItem(index);	
	}
	
	/**
	 * Returns a list which includes the items added to the current order
	 * @return List with Order's items.
	 */
	public List<Item> getItemsOrder(){
		return order.getItems();	
	}
	
	/**
	 * Returns a list which includes the items added to the current order
	 * @return List with Order's items.
	 */
	public String showOrder(){
		StringBuilder text = new StringBuilder();
		text.append("\n*********This is your order***********\n");
		text.append(order);
		return text.toString();
	}
	
	/**
	 * Returns the total gross cost of the current order
	 * @return Gross cost of the order.
	 */
	public double getOrderTotalGrossCost(){
		return order.getTotalGrossCost();
	}
	
	/**
	 * Commit the current order
	 * @throws Exception When the order has already been committed or decrease1Stock throws an ItemException.
	 */
	public void commitOrder() throws Exception{
		order.commit();
	}
	
	
	/**
	 * Returns a List sorted by item's name (first numbers, after characters, etc.) with those items from the inventory that match the category indicated.
	 * If the category does not match with "MAINCOURSE", "SIDE", "BEVERAGE" or "DESSERT", then this method returns the whole inventory list sorted by name.  
	 *
	 * Note: We recommend using Java 8's lambda expressions to do this method. However, using lamda expressions is not mandatory.
	 * See: https://dzone.com/articles/using-lambda-expression-sort
	 *
	 *@param category Item's category. The available categories are: "MAINCOURSE", "SIDE", "BEVERAGE" and "DESSERT".
	 *@return List with Order's items sorted by name and filtered by category. 
	 */
	public List<Item> getInventoryPerCategory(String category){
		
		List<Item> sortedList = new LinkedList<Item>();
		
		//First we will filter the items and add them to a single list of the chosen type
				
		if(category == "MAINCOURSE") {
			for(int i = 0; i < inventory.getList().size(); i++) {
				if(inventory.getList().get(i) instanceof MainCourse) {
					sortedList.add(inventory.getList().get(i));
				}
			}
			
			}else if(category == "SIDE") {
				for(int i = 0; i < inventory.getList().size(); i++) {
					if(inventory.getList().get(i) instanceof Side) {
						sortedList.add(inventory.getList().get(i));
					}
				}
				
				}else if(category == "BEVERAGE") {
					for(int i = 0; i < inventory.getList().size(); i++) {
						if(inventory.getList().get(i) instanceof Beverage) {
							sortedList.add(inventory.getList().get(i));
						}
					}
			
					}else if(category == "DESSERT") {
						for(int i = 0; i < inventory.getList().size(); i++) {
							if(inventory.getList().get(i) instanceof Dessert) {
								sortedList.add(inventory.getList().get(i));
							}
						}
						
					}else {
						for(int i = 0; i < inventory.getList().size(); i++) {
							sortedList.add(inventory.getList().get(i));
							}
						}
		
		//We sort the list by name using Java 8's lambda expressions
		sortedList.sort((item1,item2) -> item1.getName().compareTo(item2.getName()));
		sortedList.forEach(System.out::println);
		
		return sortedList; //Ponemos "null" para que no d� error el compilador, deber�s cambiarlo.
	}	
}
