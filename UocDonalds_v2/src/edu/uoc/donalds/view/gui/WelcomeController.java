package edu.uoc.donalds.view.gui;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

import javax.xml.datatype.Duration;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * This class is the controller of the Welcome View.
 * 
 * @author David Garc�a Sol�rzano
 * @version 1.0
 */
public class WelcomeController extends Controller{
	
	
	public WelcomeController() {		
		super();
	}

	/**
	 * Called when the user clicks on the screen.
	 */
	@FXML
	private void handleStartButton(){
		guiApp.getKiosk().createOrder();
		guiApp.goToScene("DiningLocationView.fxml");		
	}
	
	@FXML private Label timeLabel;

	private int minute;
	private int hour;
	private int second;
	private int day, month, year;

	/**
	 * This method shows the current time on the screen
	 */
	@FXML
	public void initialize() {

	    Thread clock = new Thread() {
	        public void run() {
	            for (;;) {
	                Calendar cal = Calendar.getInstance();
	                day = cal.get(Calendar.DAY_OF_MONTH);
	                month = cal.get(Calendar.MONTH);
	                year = cal.get(Calendar.YEAR);
	                second = cal.get(Calendar.SECOND);
	                minute = cal.get(Calendar.MINUTE);
	                hour = cal.get(Calendar.HOUR);
	                //System.out.println(hour + ":" + (minute) + ":" + second);
	               // timeLabel.setText(day + "of" + month + "of" + year + "\n");

	                timeLabel.setText(hour + ":" + minute + ":" + second);

	                try {
	                    sleep(1000);
	                } catch (InterruptedException ex) {
	                     //...
	                }
	            }
	        }
	    };
	    clock.start();
	}
	
	
	
}
