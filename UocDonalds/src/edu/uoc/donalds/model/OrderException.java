package edu.uoc.donalds.model;

/**
 * This class customizes the exceptions of the Order class
 * 
 * @author Juan
 * 
 * @version 1.0
 */
@SuppressWarnings("serial")
public class OrderException extends Exception{

	/**
	 * Default Constructor
	 */
	public OrderException() {
		super();
	}
	
	/**
	 * Constructor with 1 argument.
	 * 
	 * @param msg Message we want to display.
	 */
	public OrderException(String msg) {
		super(msg);		
	}

}
