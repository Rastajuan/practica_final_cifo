package edu.uoc.donalds.view.gui;

import javafx.fxml.FXML;

/**
 * This class is the controller of the Welcome View.
 * 
 * @author David Garc�a Sol�rzano
 * @version 1.0
 */
public class WelcomeController extends Controller{

	public WelcomeController() {		
		super();
	}

	/**
	 * Called when the user clicks on the screen.
	 */
	@FXML
	private void handleStartButton(){
		guiApp.getKiosk().createOrder();
		guiApp.goToScene("DiningLocationView.fxml");		
	}	
}
